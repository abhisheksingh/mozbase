# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.


import re
import os
import unittest
import subprocess
import moznetwork.moznetwork


class TestMoznetwork(unittest.TestCase):
    """Test class to test moznetwork module """

    def test_get_lan_ip(self):
        if os.name != 'nt':
            command = 'ifconfig'
            look_patt = r'inet'
        else:
            command = 'ipconfig'
            look_patt = r'IPv4 Address'

        p = subprocess.Popen([command], stdout=subprocess.PIPE)
        content = p.communicate()
        ip_patt = r'[\s\.\:]*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        ip_regex = look_patt + ip_patt
        patt = re.compile(ip_regex)
        ip_list = patt.findall(content[0])

        self.assertIn(moznetwork.get_lan_ip(), ip_list,
                      "get_lan_ip() failed !")

if __name__ == '__main__':
    unittest.main()
